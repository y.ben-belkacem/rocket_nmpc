# rocket_nmpc

Acado Toolkit solvers testing

# Install the toolkit


```
git clone https://gitlab.isae-supaero.fr/y.ben-belkacem/rocket_nmpc
cd rocket_nmpc/acadolib/build/
cmake .. -DBUILD_SHARED_LIBS=ON
cmake --build . --target install

```
# Build the application

```
cd ../../workspace/build
cmake ..
make
./nmpc_control
```

# Code source location

The folder workspace/src contains the source file **control.cpp** where the nmpc formulation and solving is written.

The folder **acadolib** is the AcadoToolkit itself.

