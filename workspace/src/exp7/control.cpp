/*
 *    This file is part of ACADO Toolkit.
 *
 *    ACADO Toolkit -- A Toolkit for Automatic Control and Dynamic Optimization.
 *    Copyright (C) 2008-2014 by Boris Houska, Hans Joachim Ferreau,
 *    Milan Vukov, Rien Quirynen, KU Leuven.
 *    Developed within the Optimization in Engineering Center (OPTEC)
 *    under supervision of Moritz Diehl. All rights reserved.
 *
 *    ACADO Toolkit is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 3 of the License, or (at your option) any later version.
 *
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with ACADO Toolkit; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */



 /**
 *    \file   examples/simulation_environment/getting_started.cpp
 *    \author Hans Joachim Ferreau, Boris Houska
 */


#include <acado_toolkit.hpp>
#include <acado_optimal_control.hpp>
#include <acado_gnuplot.hpp>
#include <acado/matrix_vector/matrix_vector.hpp>
#include <chrono>
#include <math.h>

#define DEG2RAD 3.141592/180.0
#define RAD2DEG 180.0/3.141592

int main()
{

    USING_NAMESPACE_ACADO

    // Parameters
    double m = 1.2;
    double tf = 5;

    //maximum tilt
    float max_tilt = 20*DEG2RAD;

    double gravity = 9.81;

    //gravity
	Expression g(3,1);
	g(0,0) = -gravity;
	g(1,0) = 0;
	g(2,0) = 0;
    // Gravity Center
    Expression rG(3,1);
	rG(0,0) = 0.3;
	rG(1,0) = 0;
	rG(2,0) = 0;
    //Inertia
    float J1,J2,J3;
    J1 = 0.022; J2 = 0.065; J3=J2;
    DMatrix J = zeros<double>(3,3);
    J(0,0) = J1;		
	J(1,1) = J2;
	J(2,2) = J3;
    // inv J
    DMatrix Jinv= zeros<double>(3,3);
    Jinv(0,0) = 1/J1;		
	Jinv(1,1) = 1/J2;		
	Jinv(2,2) = 1/J3;


    // INTRODUCE THE VARIABLES:
    // -------------------------
	// Position 
	DifferentialState p("",3,1);
	// velocity 
	DifferentialState v("",3,1);
    // quaternion
    DifferentialState q("",4,1);
    // omega
    DifferentialState w("",3,1);
	//Thrust
	Control T("",3,1);


    // Intermediate states computed from main states

    // Quaternion decomposition
	
    //IntermediateState q(1)(q(1,0)), q(2)(q(2,0)), q(3)(q(3,0)), q(0)(q(0,0));
    // Rotation matrix
    IntermediateState rot(3,3);
	rot(0,0) = (1 - 2*(q(2)*q(2) + q(3)*q(3)));		rot(0,1) = (2*(q(1)*q(2) - q(3)*q(0)));			rot(0,2) = (2*(q(1)*q(3) + q(2)*q(0)));
	rot(1,0) = (2*(q(1)*q(2) + q(3)*q(0)));			rot(1,1) = (1 - 2*(q(1)*q(1) + q(3)*q(3)));		rot(1,2) = (2*(q(2)*q(3) - q(1)*q(0)));
	rot(2,0) = (2*(q(1)*q(3) - q(2)*q(0)));			rot(2,1) = (2*(q(2)*q(3) + q(1)*q(0)));			rot(2,2) = (1 - 2*(q(1)*q(1) + q(2)*q(2)));
    // omega decomposition
    //IntermediateState w(0)(w(0,0)),w(1)(w(1,0)),w(2)(w(2,0));  
    // Propagation matrix
    
    IntermediateState Qpropagation(4,4);
    Qpropagation(0,0) = 0    ;   Qpropagation(0,1) = -w(0)  ;   Qpropagation(0,2) = -w(1) ;    Qpropagation(0,3) = -w(2);
    Qpropagation(1,0) = w(0)   ;   Qpropagation(1,1) =  0   ;   Qpropagation(1,2) =  w(2) ;    Qpropagation(1,3) = -w(1);
    Qpropagation(2,0) = w(1)   ;   Qpropagation(2,1) = -w(2)  ;   Qpropagation(2,2) =   0 ;    Qpropagation(2,3) =  w(0);
    Qpropagation(3,0) = w(2)   ;   Qpropagation(3,1) =  w(1)  ;   Qpropagation(3,2) = -w(0) ;    Qpropagation(3,3) =  0 ;
   
    // cross T
     
    IntermediateState crossT(3,3);
    crossT(0,0) =  0    ; crossT(0,1) = -T(2) ; crossT(0,2) = T(1); 
    crossT(1,0) =  T(2) ; crossT(1,1) =   0   ; crossT(1,2) =-T(0);
    crossT(2,0) = -T(1) ; crossT(2,1) =  T(0) ; crossT(2,2) =  0  ;

    IntermediateState crossw(3,3);
    crossw(0,0) =  0    ; crossw(0,1) = -w(2) ; crossw(0,2) = w(1); 
    crossw(1,0) =  w(2) ; crossw(1,1) =   0   ; crossw(1,2) =-w(0);
    crossw(2,0) = -w(1) ; crossw(2,1) =  w(0) ; crossw(2,2) =  0  ;

    DifferentialEquation f(0.0, tf);
	f << dot(p) == v;
    f << dot(v) ==  (1/m)*T+ g;//(1/m)*rot*T + g;
    f << dot(q) == (0.5) * Qpropagation * q;
    f << dot(w) == Jinv*(crossT*rG - crossw*J*w);


    // SETTING UP THE (SIMULATED) PROCESS:
    // -----------------------------------

	OutputFcn identity;
	DynamicSystem dynamicSystem(f ,identity);
	Process process(dynamicSystem, INT_RK45);

    // DEFINE AN OPTIMAL CONTROL PROBLEM:
    // ----------------------------------
    Function h;
    //h << p(0,0);
    h << p(0);
    //h << q(0);
    h << q(1)*q(0) - q(2)*q(3);
    h << q(1)*q(2) + q(0)*q(3);
    //h << w;
	//h << tf;
    //DMatrix Q = zeros<double>(9,9); // LSQ coefficient matrix
	DMatrix Q = zeros<double>(3,3); // LSQ coefficient matrix
	
    Q(0,0) = 1.0;//10.0;
	Q(1,1) = 1.0;//0.5;
	Q(2,2) = 1.0;//0.5;
    Q(3,3) = 1.0;//0.5;
    
    /*
    Q(3,3) = 0.5;
    Q(4,4) = 5.0;
    Q(5,5) = 5.0;
    Q(6,6) = 1.0;
    Q(7,7) = 1.0;
    Q(8,8) = 1.0;
    */
    //DVector r(9); // Reference
    DVector r(3); // Reference
    r.setAll( 0.0 );
    //r(0) = .0;

    const double tStart = 0.0;
    OCP ocp( tStart, tf, 50);
    ocp.minimizeLSQ( Q, h, r );
	ocp.subjectTo( f );

    //initial conditions
    DVector pinit(3),vinit(3),qinit(4),winit(3);
    qinit(0) = 1; qinit(1) = 0; qinit(2) = 0; qinit(3) = 0;
    pinit(0) = 15.0; pinit(1) = 5.0; pinit(2) = 5.0;
    vinit(0) = 0 ; vinit(1) = 0; vinit(2) = 0;
    winit(0) = 0.1 ; winit(1) = 0.1; winit(2) = 0.1;
    
    ocp.subjectTo(AT_START, p == pinit);
    ocp.subjectTo(AT_START, v == vinit);
    ocp.subjectTo(AT_START, w == winit);
    ocp.subjectTo(AT_START, q == qinit);
    


    //final condition
    DVector pfinal(3),vfinal(3), qfinal(4), wfinal(3);
    pfinal(0) = 0.05; pfinal(1) = 50; pfinal(2) = 50;
    vfinal(0) = 6; vfinal(1) = 6; vfinal(2) = 6;
    wfinal(0) = 1; wfinal(1) = 1; wfinal(2) = 1;
    qfinal(0) = 1; qfinal(1) = 0; qfinal(2) = 0, qfinal(3)=0;
    
    //ocp.subjectTo(AT_END, -pfinal <= p <= pfinal);
    //ocp.subjectTo(AT_END, -vfinal <= v <= vfinal);


    //ocp.subjectTo(AT_END, -wfinal <= w <= wfinal);
    //ocp.subjectTo(AT_END, q == qfinal);
    
    // constraints
    //IntermediateState pitch, yaw;
    float maxAngle = 10*DEG2RAD;
    ocp.subjectTo( -sin(maxAngle) <= 2*(q(2)*q(3) - q(0)*q(1)) <= sin(maxAngle) );
    ocp.subjectTo( -tan(maxAngle) <= -2*(q(1)*q(2) + q(3)*q(0))  /  (1 - q(1)*q(1) - q(3)*q(3))   <= tan(maxAngle));


    float minTx = m*gravity*cos(max_tilt);
    float maxTx = m*gravity;

    float Tlat = m*gravity*sin(max_tilt);

    ocp.subjectTo(  minTx <= T(0) <=maxTx);
    ocp.subjectTo(  -Tlat <= T(1) <= Tlat);
    ocp.subjectTo(  -Tlat <= T(2) <= Tlat);
    
    //ocp.subjectTo(  (m*gravity) - 0.05 <= sqrt(T(0)*T(0)+T(1)*T(1)+T(2)*T(2)) <= (m*gravity) + 0.05);
    
    //ocp.subjectTo( -Tlat <= T(1) <= Tlat);
    //ocp.subjectTo( -Tlat <= T(2) <= Tlat );
    
    //ocp.subjectTo(-ones<double>(3,1)*0.6 <= p <= ones<double>(3,1)*6);
    ocp.subjectTo( 0.99 <=(q(0)*q(0)  + q(1)*q(1) + q(2)*q(2) + q(3)*q(3)) <=  1.01);
    //ocp.subjectTo(-2 <= p(0) <= 50); 
    //ocp.subjectTo(-100 <= p(1) <= 100);
    //ocp.subjectTo(-100 <= p(2) <= 100);

    //ocp.subjectTo( 0 <= T(0));
    //ocp.subjectTo( -5 <= v(0) <= 5);
    ocp.subjectTo( -1 <= q(0) <= 1);
    ocp.subjectTo( -1 <= q(1) <= 1);
    ocp.subjectTo( -1 <= q(2) <= 1);
    ocp.subjectTo( -1 <= q(3) <= 1);
    //ocp.subjectTo(   -6*ones<double>(3,1) <=v<= 6*ones<double>(3,1) );
    
    OptimizationAlgorithm algorithm(ocp);
	
	algorithm.set( INTEGRATOR_TYPE, INT_RK45 );
	algorithm.set( DISCRETIZATION_TYPE, SINGLE_SHOOTING );
	//algorithm.set( DYNAMIC_SENSITIVITY,FORWARD_SENSITIVITY );
	algorithm.set(QP_SOLVER,QP_QPOASES);
	//algorithm.set(RET_RELAXING_QP,);
	algorithm.set( INTEGRATOR_TOLERANCE, 1e-1);
    algorithm.set( KKT_TOLERANCE, 1e-2 );



/*
 	GnuplotWindow window;

	window.addSubplot( p(0),   "Body Position X [m]" );
 	window.addSubplot( p(1),   "Body Position Y [m]" );
	window.addSubplot( p(2),   "Body Posiition Z [m]");
	        
 	window.addSubplot( v(0),   "Velocity X [m/s]" );
	window.addSubplot( v(1),   "Velocity Y [m/s]" );
	window.addSubplot( v(2),   "Velocity z [m/s]" );

	window.addSubplot( T(0),   "Thrust X [N]" );
	window.addSubplot( T(1),   "Thrust Y [N]" );
	window.addSubplot( T(2),   "Thrust Z [N]" );

    window.addSubplot( asin(2*(q(2)*q(3) - q(0)*q(1)))*RAD2DEG,   "Pitch [deg]" );
	window.addSubplot( atan(-2*(q(1)*q(2) + q(3)*q(0))  /  (1 - q(1)*q(1) - q(3)*q(3)))*RAD2DEG,   "Yaw  [deg]" );
	//window.addSubplot( T(2),   "Thrust Z [N]" );

	algorithm << window;
*/

	auto start = std::chrono::high_resolution_clock::now();


		algorithm.solve();
	

	auto elapsed = std::chrono::high_resolution_clock::now() -start;
	long long microseconds = std::chrono::duration_cast<std::chrono::milliseconds>(
        elapsed).count();

	algorithm.getDifferentialStates("./states.txt");
	algorithm.getControls("./controls.txt");

	std::cout << "Elapsed time : "<< microseconds <<" ms"<< std::endl;

    std::cout << "minTX : "<< minTx << std::endl;
    

        return EXIT_SUCCESS;

}


