import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


lines = []
with open("states.txt") as f:
    
    while (True):
        a = f.readline().replace("\n","").replace("]","").replace("[","").split()
        if (len(a) < 1):
            break
        lines.append(a)
    
for i in range(np.shape(lines)[0]):
    for j in range(np.shape(lines)[1]):
        lines[i][j] = float(lines[i][j])
        
data = np.array(lines)


fig = plt.figure()
ax = plt.axes(projection='3d')

# Data for a three-dimensional line
zline = data[:,1]
xline = data[:,2]
yline = data[:,3]
ax.plot3D(xline, yline, zline, 'blue')

plt.title("Optimized landing trajectory")
ax.set_xlabel("Y")
ax.set_ylabel("Z")
ax.set_zlabel("X")

ax.text(data[0,1], data[0,2], data[0,3], "Init")
ax.text(data[10,1], data[10,2], data[10,3], "Final")



plt.savefig("trajectory.png")
plt.show()
