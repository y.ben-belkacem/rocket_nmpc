from parse import parse
import numpy as np
from matplotlib import pyplot as plt

print("Reading file timedata.txt ...")
try:
	f = open("timedata.txt",'r')
except:
	print("Cannot open timedata.txt")
	exit()
	
times = []

while(1):
	
	line = f.readline().replace("\n","")
	if len(line) < 1:
		break
	
	time = parse("Elapsed time : {} ms",line)
	
	times.append(int(time[0]))
	

print("Read ",len(times)," Times")

f.close()

print("sd = ",np.sqrt(np.var(times)))
print("mean = ",np.mean(times))
print("min = ",np.min(times))
print("max = ",np.max(times))
plt.hist(times, density=False, bins=250)
plt.ylabel("Occurences")
plt.xlabel("Times : us")
plt.title("Execution time distribution on 1000 samples")
plt.grid("on",color='r', linestyle='-', linewidth=0.3,fillstyle="full")
plt.show()


