/*
 *    This file is part of ACADO Toolkit.
 *
 *    ACADO Toolkit -- A Toolkit for Automatic Control and Dynamic Optimization.
 *    Copyright (C) 2008-2014 by Boris Houska, Hans Joachim Ferreau,
 *    Milan Vukov, Rien Quirynen, KU Leuven.
 *    Developed within the Optimization in Engineering Center (OPTEC)
 *    under supervision of Moritz Diehl. All rights reserved.
 *
 *    ACADO Toolkit is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 3 of the License, or (at your option) any later version.
 *
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with ACADO Toolkit; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */



 /**
 *    \file   examples/simulation_environment/getting_started.cpp
 *    \author Hans Joachim Ferreau, Boris Houska
 */


#include <acado_toolkit.hpp>
#include <acado_optimal_control.hpp>
#include <acado_gnuplot.hpp>
#include <acado/matrix_vector/matrix_vector.hpp>
#include <chrono>
#include <math.h>


int main()
{

    USING_NAMESPACE_ACADO

    // INTRODUCE THE VARIABLES:
    // -------------------------
	// Position 
	DifferentialState p("",3,1);
	//DifferentialState x,y,z;
	// velocity; 
	DifferentialState v("",3,1);//,vy,vz;
	//Thrust
	Control T("",3,1);
	//Control Ty;
	//Control Tz;
	//Parameter eta;
	//Parameter tf;

	double tf = 3;
	double m = 1;
	double vmax = 3;
	double Tmax = 12;
	double Tmin = 8;
	//double g = -9.81;

	Expression g(3,1);
	g(0,0) = -9.81;
	g(1,0) = 0;
	g(2,0) = 0;

	//IntermediateState dvx(3,1);
	//IntermediateState T(3,1);
	
	
	double lambda1 = 1;
	double lambda2 = 2;

	
	
	//DMatrix rot(3,3);
	/*
	IntermediateState rot(3,3);
	IntermediateState qx(q(1)), qy(q(2)), qz(q(3)), qs(q(0));
	rot(0,0) = (1 - 2*(qy*qy + qz*qz));		rot(0,1) = (2*(qx*qy - qz*qs));			rot(0,2) = (2*(qx*qz + qy*qs));
	rot(1,0) = (2*(qx*qy + qz*qs));			rot(1,1) = (1 - 2*(qx*qx + qz*qz));		rot(1,2) = (2*(qy*qz - qx*qs));
	rot(2,0) = (2*(qx*qz - qy*qs));			rot(2,1) = (2*(qy*qz + qx*qs));			rot(2,2) = (1 - 2*(qx*qx + qy*qy));

*/
	
    // DEFINE A DIFFERENTIAL EQUATION:
    // -------------------------------
    DifferentialEquation f(0.0, tf);

	f << dot(p) == v;
	//f << dot(y) == v(1);
	//f << dot(z) == v(2);
	/*
	T(0,0) = Tx;
	T(1,0) = Ty;
	T(2,0) = Tz;
*/


	f << dot(v) == (1/m)*T+g;//(1/m)*Tx + g;
	//f << dot(vy) == dvx(1,0);//(1/m)*Ty ;
	//f << dot(vz) == dvx(2,0);//(1/m)*Tz ;

	/*
	Expression dvx(3,1);
	Expression T(3,1);
	T(0,0) = Tx; T(1,0) = Ty; T(2,0)= Tz;	
*/
/*
	Expression qinit(4,1);
	qinit(0) = 1;
	qinit(1) = 0;
	qinit(2) = 0;
	qinit(3) = 0;
*/
	/*
	f << dot(Tx) == dTx;
	f << dot(Ty) == dTy;
	f << dot(Tz) == dTz;
	*/

    // SETTING UP THE (SIMULATED) PROCESS:
    // -----------------------------------
	OutputFcn identity;
	DynamicSystem dynamicSystem(f ,identity);

	Process process(dynamicSystem, INT_RK45);

    // DEFINE AN OPTIMAL CONTROL PROBLEM:
    // ----------------------------------
    Function h;

	//h << x;
	//h << y;
	//h << z;

    h << T;
   // h << Ty;
   // h << Tz;

	//h << tf;
  
    DMatrix Q = zeros<double>(3,3); // LSQ coefficient matrix
	Q(0,0) = 1.0;
	Q(1,1) = 1.0;
	Q(2,2) = 1.0;

    DVector r(3); // Reference
    r.setAll( 0.0 );

    const double tStart = 0.0;

    OCP ocp( tStart, tf, 30);
    ocp.minimizeLSQ( Q, h, r );
	ocp.subjectTo( f );

	//Initial position

	// ocp.subjectTo(AT_START, q(0,0) == 1);
	// ocp.subjectTo(AT_START, q(1,0) == 0);
	// ocp.subjectTo(AT_START, q(2,0) == 0);
	// ocp.subjectTo(AT_START, q(3,0) == 0);
	
	ocp.subjectTo(AT_START, p(0) == 5.0);
	ocp.subjectTo(AT_START, p(1) == 5.0);
	ocp.subjectTo(AT_START, p(2) == 5.0);

	//Initial velocity
	ocp.subjectTo(AT_START, v(0)== 0.0);
    ocp.subjectTo(AT_START, v(1) == 0.0);
    ocp.subjectTo(AT_START, v(2) == 0.0);

	ocp.subjectTo(AT_END, -0.05 <= p(0) <= 0.05);
	ocp.subjectTo(AT_END, -0.05 <= p(1) <= 0.05);
	ocp.subjectTo(AT_END, -0.05 <= p(2) <= 0.05);

	ocp.subjectTo(AT_END, -0.05 <= v(0) <= 0.05);
    ocp.subjectTo(AT_END, -0.05 <=  v(1) <=0.05);
    ocp.subjectTo(AT_END, -0.05 <= v(2) <= 0.05);

	
	ocp.subjectTo(-0.06 <= p(0) <= 6);
	ocp.subjectTo(-0.06 <= p(1) <= 6);



	//Gliding slop cone constraint 
	//ocp.subjectTo( (y*y + z*z - x*x) <= 0 );
	
	//Thrust constraint
	//ocp.subjectTo( Tx <= 15);

	//ocp.subjectTo(Tmin <= Ty <= Tmax);

	
	
	//velocity constraint
	//ocp.subjectTo( (sqrt(vx*vx + vy*vy + vz*vz)) <= (vmax));

	//Tilt Constraint
	//ocp.subjectTo( (0.93*sqrt(Tx*Tx + Ty*Ty + Tz*Tz) - Tx) <= 0  );

	
	//Time constraint
	//ocp.subjectTo(2 <= tf <= 10);


    // SETTING UP THE MPC CONTROLLER:
    // ------------------------------
	OptimizationAlgorithm algorithm(ocp);
	
	algorithm.set( INTEGRATOR_TYPE, INT_RK45 );
	algorithm.set( DISCRETIZATION_TYPE, SINGLE_SHOOTING );
	//algorithm.set( DYNAMIC_SENSITIVITY,FORWARD_SENSITIVITY );
	algorithm.set(QP_SOLVER,QP_QPOASES);
	//algorithm.set(RET_RELAXING_QP,);
	algorithm.set( INTEGRATOR_TOLERANCE, 1e-3 );
    algorithm.set( KKT_TOLERANCE, 1e-2 );




 	GnuplotWindow window;

	window.addSubplot( p(0),   "Body Position X [m]" );
 	window.addSubplot( p(1),   "Body Position Y [m]" );
	window.addSubplot( p(2),   "Body Posiition Z [m]");
	        
 	window.addSubplot( v(0),   "Velocity X [m/s]" );
	window.addSubplot( v(1),   "Velocity Y [m/s]" );
//	window.addSubplot( vz,   "Velocity z [m/s]" );
	window.addSubplot( T(0),   "Thrust X [N]" );
	window.addSubplot( T(2),   "Thrust Y [N]" );
//	window.addSubplot( Tz,   "Thrust Z [N]" );

	algorithm << window;


	auto start = std::chrono::high_resolution_clock::now();


		algorithm.solve();
	
        


	auto elapsed = std::chrono::high_resolution_clock::now() -start;
	long long microseconds = std::chrono::duration_cast<std::chrono::milliseconds>(
        elapsed).count();

	algorithm.getDifferentialStates("./states.txt");
	algorithm.getControls("./controls.txt");

	std::cout << "Elapsed time : "<< microseconds <<" ms"<< std::endl;

/*
// 	algorithm.set( MAX_NUM_ITERATIONS, 2 );
    algorithm.set( USE_IMMEDIATE_FEEDBACK,YES );

	StaticReferenceTrajectory zeroReference;

	Controller controller(algorithm,zeroReference);


    // SETTING UP THE SIMULATION ENVIRONMENT,  RUN THE EXAMPLE...
    // ----------------------------------------------------------
	SimulationEnvironment sim( 0.0,10,process,controller);

	DVector x0(6);
	x0.setZero();
	x0(0) = 5;
	x0(1) = 5;

	if (sim.init( x0 ) != SUCCESSFUL_RETURN)
		exit( EXIT_FAILURE );
	if (sim.run( ) != SUCCESSFUL_RETURN)
		exit( EXIT_FAILURE );


    // ... AND PLOT THE RESULTS
    // ------------------------
 	VariablesGrid diffStates;
 	sim.getProcessDifferentialStates( diffStates );

 	VariablesGrid feedbackControl;
 	sim.getFeedbackControl( feedbackControl );

 	GnuplotWindow window;
 	window.addSubplot( diffStates(0),   "Body Position X [m]" );
 	window.addSubplot( diffStates(1),   "Body Position Y [m]" );
 	window.addSubplot( diffStates(2),   "Body Position Z [m]" );
 	window.addSubplot( diffStates(3),   "Velocity X [m/s]" );
 	window.addSubplot( feedbackControl(0), "Thrust X [N]" );
	window.addSubplot( feedbackControl(1), "Thrust Y [N]" );
 	window.plot( );
*/
	//std::cout << diffStates(0) << std::endl;

    return EXIT_SUCCESS;
}



